﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace MTJ.Selenium
{
    public class WebDriver : IDisposable
    {
        private const int MAX_TRIES = 10;

        public IWebDriver Driver;

        public WebDriver(Browser browser, object options = null)
        {
            Type type = null;
            switch (browser)
            {
                case Browser.Chrome: type = typeof(OpenQA.Selenium.Chrome.ChromeDriver);break;
            }
            if (options == null)
                Driver = (IWebDriver)Activator.CreateInstance(type);
            else
                Driver = (IWebDriver)Activator.CreateInstance(type, options);
            Driver.Manage().Window.Maximize();
        }

        public IWebElement FindElement(string selector)
        {
            IWebElement el = null;
            try { el = Driver.FindElement(By.CssSelector(selector)); }
            catch { }
            return el;
        }
        public IWebElement[] FindElements(string selector)
        {
            return Driver.FindElements(By.CssSelector(selector)).ToArray();
        }

        public object ExecuteScript(string script)
        {
            return ((IJavaScriptExecutor)Driver).ExecuteScript(script);
        }

        public T ExecuteScript<T>(string script)
        {
            return (T)ExecuteScript(script);
        }

        public void Navigate(string url)
        {
            Driver.Navigate().GoToUrl(url);
        }

        public void SendKeys(string selector, string text)
        {
            FindElement(selector).SendKeys(text);
        }

        public void Click(string selector)
        {
            FindElement(selector).Click();
        }

        public void WaitFor(Func<bool> func, int maxTries = MAX_TRIES)
        {
            int loops = 0;
            Action wait = null;
            wait = () =>
            {
                Thread.Sleep(500);
                if (!func())
                {
                    if (maxTries > 0 && loops == maxTries)
                        throw new Exception("Max wait loops reached.");
                    else
                    {
                        loops++;
                        wait();
                    }
                }
            };
            wait();
        }
        public IWebElement WaitForElement(string selector, int maxTries = MAX_TRIES)
        {
            IWebElement element = null;
            WaitFor(() => (element = FindElement(selector)) != null, maxTries);
            return element;
        }

        public void ScrollTo(long x, long y)
        {
            ExecuteScript($"window.scrollTo({x}, {y})");
        }
        public long ScrollToBottom()
        {
            var bottom = ExecuteScript<long>("return document.body.scrollHeight");
            ScrollTo(0, bottom);
            return bottom;
        }

        public void MoveTo(IWebElement element)
        {
            var actions = new Actions(Driver);
            actions.MoveToElement(element).Perform();
        }

        public void Dispose()
        {
            Driver.Dispose();
        }
    }

    public static class WebDriverExtensions
    {
        public static IWebElement FindElement(this IWebElement element, string selector)
        {
            IWebElement el = null;
            try { el = element.FindElement(By.CssSelector(selector)); }
            catch { }
            return el;
        }

        public static IWebElement[] FindElements(this IWebElement element, string selector)
        {
            return element.FindElements(By.CssSelector(selector)).ToArray();
        }
    }
}
