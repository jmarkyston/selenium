for file in *.nuspec; do
  filename="$(basename "$file" .nuspec).nuspec"
  nuget pack $filename
done
for file in *.nupkg; do
  filename="$(basename "$file" .nupkg).nupkg"
  nuget add $filename -source ~/documents/projects/nuget
  rm $filename
done