using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;

namespace MTJ.Selenium.Test
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void Construct()
        {
            var options = new ChromeOptions();
            using (var driver = new WebDriver(Browser.Chrome, options))
            {
                Assert.IsNotNull(driver);
            }
        }
    }
}
